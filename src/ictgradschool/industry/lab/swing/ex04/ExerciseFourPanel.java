package ictgradschool.industry.lab.swing.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private Balloon balloon;
    private JButton moveButton;
    private Timer timer;
    private Timer othertimer;
    ArrayList <Balloon> arrayList;

    /**
     * Creates a new ExerciseFourPanel.
     */
//    public ExerciseFourPanel() {
//        setBackground(Color.white);
//
//        this.balloon = new Balloon(30, 60);
//
//        this.moveButton = new JButton("Move balloon");
//        this.moveButton.addActionListener(this);
//        this.add(moveButton);
//        this.addKeyListener (this);
//        this.timer = new Timer(200, this);
//    }
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloon = new Balloon(30, 60);
        this.addKeyListener(this);
        this.timer = new Timer(200, this);
        this.othertimer = new Timer(600,this);
        othertimer.start();
        arrayList = new ArrayList<>();

        }





    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     *
     * @param e
     */
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == timer) {

            balloon.move();
        }
if (e.getSource() == othertimer){

    int x = (int) (Math.random()* 200);
    int y = (int) (Math.random()* 200);


    arrayList.add(new Balloon(x,y));


}
        requestFocusInWindow();

        repaint();


    }

    /**
     * Draws any balloons we have inside this panel.
     *
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        balloon.draw(g);
        for (int i = 0; i < arrayList.size() ; i++) {
            arrayList.get(i).draw(g);

        }

        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        timer.start();

        if (keyCode == KeyEvent.VK_S) {
            // move left
           timer.stop();
            repaint();
        }


        if (keyCode == KeyEvent.VK_LEFT) {
            // move left
            balloon.setDirection(Direction.Left);
            balloon.move();
            repaint();
        }

        if (keyCode == KeyEvent.VK_RIGHT) {
            // move left
            balloon.setDirection(Direction.Right);
            balloon.move();
            repaint();
        }

        if (keyCode == KeyEvent.VK_UP) {
            // move left
            balloon.setDirection(Direction.Up);
            balloon.move();
            repaint();
        }

        if (keyCode == KeyEvent.VK_DOWN) {
            // move left
            balloon.setDirection(Direction.Down);
            balloon.move();
            repaint();
        }
    }
}

//
//    @Override
//    public void keyReleased(KeyEvent e) {
//        int keyCode = e.getKeyCode();
//
//        if (keyCode == KeyEvent.VK_LEFT) {
//            // move left
//            balloon.setDirection(Direction.Left);
//            balloon.move();
//            repaint();
//        }
//
//        if (keyCode == KeyEvent.VK_RIGHT) {
//            // move left
//            balloon.setDirection(Direction.Right);
//            balloon.move();
//            repaint();
//        }
//
//        if (keyCode == KeyEvent.VK_UP) {
//            // move left
//            balloon.setDirection(Direction.Up);
//            balloon.move();
//            repaint();
//        }
//
//        if (keyCode == KeyEvent.VK_DOWN) {
//            // move left
//            balloon.setDirection(Direction.Down);
//            balloon.move();
//            repaint();
//        }
//    }