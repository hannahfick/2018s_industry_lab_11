package ictgradschool.industry.lab.swing.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 * <p>
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    private JButton addButton, subtractButton;
    private JTextField num1, num2, result;
    private JLabel resultLabel;


    public ExerciseTwoPanel() {
        setBackground(Color.white);

        addButton = new JButton("Add");
        subtractButton = new JButton("Subtract");
        num1 = new JTextField(10);
        num2 = new JTextField(10);
        result = new JTextField(25);
        resultLabel = new JLabel("Result:");

        add(num1);
        add(num2);
        add(addButton);
        add(subtractButton);
        add(resultLabel);
        add(result);


        addButton.addActionListener((ActionListener) this);
        subtractButton.addActionListener(this);

    }

    //
    public void actionPerformed(ActionEvent event) {
        double n1 = Double.parseDouble(num1.getText());
        double n2 = Double.parseDouble(num2.getText());
        double n1rounded = roundTo2DecimalPlaces(n1);
        double n2rounded = roundTo2DecimalPlaces(n2);
        //        Split these guys into if statements
        if (addButton == event.getSource()) {
            double result1 = n1rounded + n2rounded;
            String sResult1 = "" + result1;

            result.setText(sResult1);
        } else if (subtractButton == event.getSource()) {

            double result2 = n1rounded - n2rounded;

            String sResult2 = "" + result2;

            result.setText(sResult2);
        }

    }

    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}


