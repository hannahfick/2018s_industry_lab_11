package ictgradschool.industry.lab.swing.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton, calculateHW;
    private JTextField heightM, weightK, yourBMIresult, maxHWresult;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.

        calculateBMIButton = new JButton("Calculate BMI");
        calculateHW = new JButton("Calculate Healthy Weight");
        heightM = new JTextField(30);
        weightK = new JTextField(30);
        yourBMIresult = new JTextField(30);
        maxHWresult = new JTextField(30);


        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        JLabel weight = new JLabel("Weight in Kilograms");
        JLabel height = new JLabel("Height in metres");
        JLabel yourBMI = new JLabel("Your Body Mass Index (BMI) is: ");
        JLabel maxHW = new JLabel("Maximum Healthy Weight for your Height");


        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)


        add(height);
        add(heightM);
        add(weight);
        add(weightK);
        add(calculateBMIButton);
        add(yourBMI);
        add(yourBMIresult);
        add(calculateHW);
        add(maxHW);
        add(maxHWresult);


        // TODO Add Action Listeners for the JButtons


        calculateBMIButton.addActionListener(this);
        calculateHW.addActionListener(this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {
        double h1 = Double.parseDouble(heightM.getText());
        double w1 = Double.parseDouble(weightK.getText());
//        Split these guys into if statements
        if (calculateBMIButton == event.getSource()) {

            double bmi = w1 / (h1 * h1);
            double roundBMI = roundTo2DecimalPlaces(bmi);

            String sBMI = "" + roundBMI;

            yourBMIresult.setText(sBMI);
        } else if (calculateHW == event.getSource()) {

            double healthWeight = 24.9 * h1 * h1;
            double roundedHW = roundTo2DecimalPlaces(healthWeight);
            String maxHealthyWeight = "" + roundedHW;

            maxHWresult.setText(maxHealthyWeight);

        }
    }// End of actionPerformed
    // TODO Implement this method.
    // Hint #1: event.getSource() will return the button which was pressed.
    // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
    // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}